﻿#define IUTEST_USE_MAIN
#include <winsock2.h>
#include "cpprefjp/random_device.hpp"
#include <iutest.hpp>
#include <cstdint>
#include <array>
namespace yumetodo {
	constexpr std::uint16_t byteswap_ushort(std::uint16_t n) { return (n & 0xff00) >> 8 | (n & 0x00ff) << 8; }
}
namespace detail {
	using seed_v_t = std::array<cpprefjp::random_device::result_type, sizeof(std::mt19937) / sizeof(cpprefjp::random_device::result_type)>;
	seed_v_t create_seed_v()
	{
		cpprefjp::random_device rnd;
		seed_v_t sed_v;
		std::generate(sed_v.begin(), sed_v.end(), std::ref(rnd));
		return sed_v;
	}
	std::mt19937 create_random_engine()
	{
		const auto sed_v = create_seed_v();
		std::seed_seq seq(sed_v.begin(), sed_v.end());
		return std::mt19937(seq);
	}
	std::mt19937& random_engine() {
		static thread_local std::mt19937 engine = create_random_engine();
		return engine;
	}
}
std::uint16_t gen() {
	auto& engine = detail::random_engine();
	std::uniform_int_distribution<std::uint16_t> dist{};
	return dist(engine);
}
#ifdef _MSC_VER
#include <intrin.h>
IUTEST_TEST(byteswap_ushort, msvc_byteswap_ushort)
{
	for (std::size_t i = 0; i < 30; ++i) {
		const auto n = gen();
		IUTEST_ASSERT_EQ(_byteswap_ushort(n), yumetodo::byteswap_ushort(n));
	}
}
IUTEST_TEST(byteswap_ushort, msvc_rotr16)
{
	for (std::size_t i = 0; i < 30; ++i) {
		const auto n = gen();
		IUTEST_ASSERT_EQ(_rotr16(n, 8), yumetodo::byteswap_ushort(n));
	}
}
#else
#include <x86intrin.h>
IUTEST_TEST(byteswap_ushort, gcc_rolw)
{
	for (std::size_t i = 0; i < 30; ++i) {
		const auto n = gen();
		IUTEST_ASSERT_EQ(__rolw(n, 8), yumetodo::byteswap_ushort(n));
	}
}
IUTEST_TEST(byteswap_ushort, gcc_rorw)
{
	for (std::size_t i = 0; i < 30; ++i) {
		const auto n = gen();
		IUTEST_ASSERT_EQ(__rorw(n, 8), yumetodo::byteswap_ushort(n));
	}
}
#endif
